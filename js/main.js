var current = 'home',
    anim_time = 300;

var blocks = $('header, footer'),
    main = $('main'),
    logo = $('.logo'),
    all = $('.slide-about, .slide-team, .slide-projects, .slide-contacts');

$('section').click(function () {
    enlarge_slide(this.id);
});

function enlarge_sections() {
    blocks.animate({
        height: '10%'
    }, anim_time);

    main.animate({
        height: '80%'
    }, anim_time);

    logo.animate({
        opacity: 0.1,
        'font-size': '12vw',
        'z-index': '1'
    }, 100)
}

function enlarge_slide(id) {
    if (current !== id && id !== 'home') {
        return_slides();
        if (current === 'home'){
            enlarge_sections();
        }
    } else if (current !== 'home') {
        current = 'home';
        return return_all();
    }

    current = id;
    $('.slide-' + id).animate({
        left: '50%',
        opacity: '1'
    }, anim_time);
}

function return_slides() {
    var left = (Math.round(Math.random())) ? '100%' : '-100%';
    all.animate({
        left: left,
        opacity: '0'
    }, anim_time)
}

function return_all() {
    return_slides();

    blocks.animate({
        height: '45%'
    }, anim_time);

    main.animate({
        height: '10%'
    }, anim_time);

    logo.animate({
        opacity: 1,
        'font-size': '2em',
        'z-index': '30'
    }, 100)
}
